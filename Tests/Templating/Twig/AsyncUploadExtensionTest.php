<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploadBundle\Tests\Templating\Twig;

use PHPUnit\Framework\TestCase;
use ChampsLibres\AsyncUploaderBundle\Templating\Twig\AsyncUploadExtension;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use Prophecy\Argument;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;


/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncUploadExtensionTest extends TestCase
{
    protected function createTempUrlGenerator()
    {
        $tempUrlGenerator = $this->prophesize();
        $tempUrlGenerator->willImplement(TempUrlGeneratorInterface::class);
        $tempUrlGenerator->generatePost(
            Argument::type('integer'), 
            Argument::type('integer')
            )->willReturn(new \stdClass());
        $tempUrlGenerator
            ->generateGet(Argument::type('string'), Argument::that(function($arg) {
                return $arg === null || is_integer($arg);
            }))
            ->will(function($args) {
                $r = new \stdClass();
                $r->url = $args[0];
                
                return $r;
            });
            
        return $tempUrlGenerator->reveal();
    }
    
    public function testComputeUrl()
    {
        $asyncUploadExtension = new AsyncUploadExtension(
           $this->createTempUrlGenerator()
           );
        $dummyAsyncFile = new class implements AsyncFileInterface {
            
            public function getObjectName()
            {
                return 'objectname';
            }
        };
        
        $this->assertEquals('objectname', $asyncUploadExtension
            ->computeUrl('objectname'));
        $this->assertEquals('objectname', $asyncUploadExtension
            ->computeUrl('objectname', 200));
        $this->assertEquals('objectname', $asyncUploadExtension
            ->computeUrl($dummyAsyncFile));
        $this->assertEquals('objectname', $asyncUploadExtension
            ->computeUrl($dummyAsyncFile, 200));
        
    }
    
    /**
     * 
     */
    public function testInvalidArgumentComputeUrl()
    {
        $asyncUploadExtension = new AsyncUploadExtension(
           $this->createTempUrlGenerator()
           );
        
        try {
            $asyncUploadExtension->computeUrl((int) 123);
        } catch (\Exception $e) {
            $this->assertTrue(true);
            
            return;
        } 
        
        $this->assertTrue(false, "the function should return an "
            . "InvalidArgumentException");
    }
}
