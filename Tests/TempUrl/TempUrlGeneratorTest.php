<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Tests\TempUrl;

use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlOpenstackGenerator;
use Psr\Log\LoggerInterface;
use Prophecy\Argument;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TempUrlGeneratorTest extends \PHPUnit\Framework\TestCase
{
    
    public function testGeneratePost()
    {
        $generator = $this->generateGenerator();
        $s = $generator->generatePost();
        
        $this->assertInstanceOf(\stdClass::class, $s);
        
        foreach([ 'signature', 'url' ] as $needle) {
            $this->objectHasAttribute($needle, $s);
        }
    }
    
    public function testGenerateGet()
    {
        $generator = $this->generateGenerator();
        $url = $generator->generateGet('object_name');
        
        $this->assertInternalType('string', $url);
        $this->assertRegExp('/^https:\/\/swift\.myopenstack\.net/', $url);
        $this->assertContains('object_name', $url);
    }
    
    protected function generateGenerator()
    {
        return new TempUrlOpenstackGenerator(
            $this->generateLogger(),
            'https://swift.myopenstack.net/', // service base url
            'account', // account id
            'container', // container name
            'KEY' // secret key
            );
    }
    
    protected function generateLogger()
    {
        $logger = $this->prophesize();
        $logger->willImplement(LoggerInterface::class);
        
        foreach ([ 'emergency', 'critical', 'alert', 'error', 'warning',
            'notice', 'info', 'debug', 'log'] as $method) {
            $logger->$method(Argument::type('string'), Argument::type('array'))
                ->willReturn(null);
        }
        
        return $logger->reveal();
    }
}
