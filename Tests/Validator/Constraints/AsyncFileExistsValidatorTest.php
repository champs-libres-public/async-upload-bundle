<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploadBundle\Tests\Validator\Constraints;

use ChampsLibres\AsyncUploaderBundle\Validator\Constraints\AsyncFileExistsValidator;
use ChampsLibres\AsyncUploaderBundle\Validator\Constraints\AsyncFileExists;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use ChampsLibres\AsyncUploaderBundle\Openstack\Client;
use Prophecy\Argument;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncFileExistsValidatorTest extends KernelTestCase
{
    protected $prophet;
    
    protected function setUp()
    {
        static::bootKernel();
        
        $this->prophet = new \Prophecy\Prophet();
    }
    
    protected function tearDown()
    {
        static::$kernel->shutdown();
    }
    
    public function testObjectNotExists()
    {
        $container = $this->prophet->prophesize();
        $container->willExtend(\OpenStack\ObjectStore\v1\Models\Container::class);
        // the container will say "object is not present"
        $container->objectExists(Argument::exact('not_present'))->willReturn(false);
        $container = $container->reveal();
        $prophet = $this->prophet;
        
        $osClient = $this->prophet->prophesize();
        $osClient->willExtend(Client::class);
        
        $osClient->objectStoreV1()
            ->will(function() use ($container, $prophet) {
                $objectStore = $prophet->prophesize(\OpenStack\ObjectStore\v1\Service::class);
                $objectStore->getContainer(Argument::exact('test'))
                ->willReturn($container);
                
                return $objectStore->reveal();
            });
        
        
        $validator = new AsyncFileExistsValidator($osClient->reveal(), 'test');
        $prophecy = $this->addExecutionContext($validator);
        
        $validator->validate($this->createAsyncFile('not_present'), new AsyncFileExists());
        
        try {
            $prophecy->buildViolation(Argument::type('string'))
                ->shouldHaveBeenCalledTimes(1);
            
            $this->assertTrue(true);
        } catch (\Prophecy\Exception\Call\UnexpectedCallException $e) {
            $this->assertTrue(false, $e->getMessage());
        }
    }
    
    protected function createAsyncFile($objectName)
    {
        return new class($objectName) implements AsyncFileInterface {
            
            protected $objectName;
            
            public function __construct($objectName)
            {
                $this->objectName = $objectName;
            }
            
            public function getObjectName() {
                return $this->objectName;
            }
        };
    }
    
    /**
     * 
     * @param AsyncFileExistsValidator $validator
     * @param type $shouldCallBuildViolation
     * @return \Prophecy\Prophecy\ProphecyInterface
     */
    protected function addExecutionContext(AsyncFileExistsValidator &$validator)
    {
        $context = $this->prophet->prophesize();
        $context->willExtend(\Symfony\Component\Validator\Context\ExecutionContext::class);
        
        $violation = $this->prophet->prophesize();
        $violation->willExtend(\Symfony\Component\Validator\Violation\ConstraintViolationBuilder::class);
        $violation->setParameter(Argument::any(), Argument::any())
            ->willReturn($violation->reveal());
        $violation->addViolation()
            ->willReturn(null)
            ;
        
        $context->buildViolation(Argument::type('string'))
            ->willReturn($violation->reveal());
        
        // here we impose the execution context although the 
        // property is not accessible...
        $reflectionValidator = new \ReflectionClass($validator);
        $prop = $reflectionValidator->getProperty('context');
        $prop->setAccessible(true);
        $prop->setValue($validator, $context->reveal());
        
        return $context;
    }
}
