<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Tests\Form\Type;

use ChampsLibres\AsyncUploaderBundle\Model\AsyncFile;
use ChampsLibres\AsyncUploaderBundle\Form\Type\AsyncUploaderFormType;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Prophecy\Argument;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncUploaderFormTypeTest extends TypeTestCase
{
    
    protected function getExtensions()
    {
        $prophet = new \Prophecy\Prophet;
        $url_generator = $prophet->prophesize();
        $url_generator->willImplement(UrlGeneratorInterface::class);
        $url_generator->setContext(Argument::any())->willReturn(null);
        $url_generator->getContext()->willReturn(null);
        $url_generator->generate(Argument::type('string'), Argument::type('array'))
            ->willReturn('/a/url');
        
        $type = new AsyncUploaderFormType($url_generator->reveal());
        
        return [
            new PreloadedExtension([$type], [])
        ];
    }
    
    public function testSubmittedData()
    {
        $data = '{ "object": "prefixed_name" } ';
        
        $expected = new AsyncFile('prefixed_name');
        
        $form = $this->factory->create(AsyncUploaderFormType::class);
        
        $form->submit($data);
        
        $this->assertTrue($form->isSynchronized());
        $this->assertInstanceOf(AsyncFile::class, $form->getData());
        $this->assertEquals($expected, $form->getData());
        
        $form->isValid();
    }
    
}
