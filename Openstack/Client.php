<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Openstack;

use OpenStack\OpenStack;
use Psr\Log\LoggerInterface;
use GuzzleHttp\MessageFormatter;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class Client extends OpenStack
{
    public function __construct(
        $auth_url,
        $region,
        $username,
        $password,
        $tenant_id,
        $debug_mode,
        LoggerInterface $logger,
        $version = '3'
        )
    {
        if ($version !== '3') {
            $config = [
                'username' => $username,
                'password' => $password,
                'authUrl'  => $auth_url,
                'region'   => $region,
                'tenantId' => $tenant_id,
                // this result in an empty service catalog
                /*
                'scope' => [
                    'project' => [
                        'id' => $tenant_id,
                    ]
                ],
                 */
                'identityService'   => \OpenStack\Identity\v2\Service::factory(
                    new \GuzzleHttp\Client( [
                        'base_uri'  => \OpenStack\Common\Transport\Utils::normalizeUrl( $auth_url ),
                        'handler'   => \GuzzleHttp\HandlerStack::create(),
                    ] ) ),
            ];
        } else {
            $config = [
                'username' => $username,
                'password' => $password,
                'authUrl'  => $auth_url,
                'region'   => $region,
                'tenantId' => $tenant_id,
                'scope' => [
                    'project' => [
                        'id' => $tenant_id,
                    ]
                ]];
        }
        
        if ($debug_mode === true) {
            $config['debugLog'] = true;
            $config['logger']   = $logger;
            $config['messageFormatter'] = new MessageFormatter(MessageFormatter::DEBUG);
        }
        
        parent::__construct($config);
        
    }
}
