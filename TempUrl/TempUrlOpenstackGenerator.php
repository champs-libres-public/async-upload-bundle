<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\TempUrl;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Generate a temp url
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TempUrlOpenstackGenerator implements TempUrlGeneratorInterface
{
    /**
     * the base url of the service
     *
     * @var string
     */
    private $base_url;
    
    private $key;
    
    private $max_expire_delay;
    
    private $max_post_file_size;
    
    private $max_file_count;
    
    private $max_submit_delay;
    
    private $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    private $logger;
    
    /**
     *
     * @var EventDispatcherInterface
     */
    private $event_dispatcher;
    
    /**
     * Indicates if the container is public.
     *
     * (not yet implemented)
     *
     * @var bool
     */
    private $isPublic = false;

    
    public function __construct(
        LoggerInterface $logger,
        EventDispatcherInterface $event_dispatcher,
        $base_url, 
        $key, 
        $max_expire_delay,
        $max_submit_delay,
        $max_post_file_size,
        $max_file_count = 1
    ) {
        $this->logger = $logger;
        $this->event_dispatcher = $event_dispatcher;
        $this->base_url = $base_url;
        $this->key = $key;
        $this->max_expire_delay = $max_expire_delay;
        $this->max_post_file_size = $max_post_file_size;
        $this->max_file_count = $max_file_count;
        $this->max_submit_delay = $max_submit_delay;
    }
    
    /**
     * 
     * 
     * @param int $expire_delay
     * @param string $max_file_count
     * @return \stdClass
     * @throws TempUrlGeneratorException
     */
    public function generatePost(
        $expire_delay = null,
        $submit_delay = null,
        $max_file_count = 1
    ) {
        $delay = $expire_delay ?? $this->max_expire_delay;
        $submit_delay = $submit_delay ?? $this->max_submit_delay;
        
        if ($delay < 2) {
            throw new TempUrlGeneratorException("The delay of $delay is too "
                . "short (<2 sec) to properly use this token");
        }
        
        if ($delay > $this->max_expire_delay) {
            throw new TempUrlGeneratorException("The given "
                . "delay is greater than the max delay authorized.");
        }
        
        if ($submit_delay < 15) {
            throw new TempUrlGeneratorException("The submit delay of $delay is too "
                . "short (<15 sec) to properly use this token");
        }
        
        if ($submit_delay > $this->max_submit_delay) {
            throw new TempUrlGeneratorException("The given "
                . "submit delay is greater than the max submit delay authorized.");
        }
        
        if ($max_file_count > $this->max_file_count) {
            throw new TempUrlGeneratorException("The number of "
                . "files is greater than the authorized number of files");
        }
        
        $expires = new \DateTime('now');
        $expires->add(new \DateInterval('PT'. (string) $delay.'S'))
            ;
 
        $object_name = $this->generateObjectName();
        $g = new \stdClass;
        
        $g->method = 'POST';
        $g->max_file_size = $this->max_post_file_size;
        $g->max_file_count = $max_file_count;
        $g->expires = (int) $expires->format('U');
        $g->submit_delay = $submit_delay;
        $g->redirect = "";
        $g->prefix = $object_name;
        $g->url = $this->generateUrl($object_name);
        $g->signature = $this->generateSignaturePost($g->url, $expires);
        
        $this->event_dispatcher->dispatch(TempUrlGenerateEvent::NAME_GENERATE, 
            new TempUrlGenerateEvent(TempUrlGeneratorInterface::POST, $g));
        
        $this->logger->info(
            'generate signature for url', (array) $g);
        
        return $g;
    }
    
    /**
     * Generate an absolute public url for a GET request on the object
     * 
     * @param string $object_name
     * @param int|null $expire_delay
     * @return string
     */
    public function generate($method, $object_name, $expire_delay = null) 
    {
        $r = new \stdClass;
        $r->method = $method;
        $url = $this->generateUrl($object_name);
        
        if ($this->isPublic) {
            $r->url = $url;
        } else {
            $expires = (new \DateTime())
                ->add(new \DateInterval(sprintf('PT%dS', $expire_delay ?? $this->max_expire_delay)));
            $args = [
                'temp_url_sig' => $this->generateSignature($method, $url, $expires),
                'temp_url_expires' => $expires->format('U')
            ];
            $r->url = $this->generateUrl($object_name).'?'.\http_build_query($args);
        }
        
        $this->event_dispatcher->dispatch($method, 
            new TempUrlGenerateEvent(TempUrlGeneratorInterface::POST, $r));
        
        return $r;
    }
    
    protected function generateUrl($relative_path)
    {
        return $this->base_url
            .$relative_path
            ;
    }
    
    protected function generateObjectName()
    {
        // inspiration from https://stackoverflow.com/a/4356295/1572236
        $charactersLength = strlen($this->characters);
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $this->characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    protected function generateSignaturePost($url, \DateTime $expires)
    {
        $path = \parse_url($url, PHP_URL_PATH);

        $body = sprintf(
            "%s\n%s\n%s\n%s\n%s",
            $path,
            "", // redirect is empty
            (string) $this->max_post_file_size,
            (string) $this->max_file_count,
            (string) $expires->format('U')
            )
            ;
            
        $this->logger->debug('generate signature post', 
            [ 'url' => $body, 'method' => 'POST' ]);
        
        return \hash_hmac('sha1', $body, $this->key, false);
    }
    
    protected function generateSignature($method, $url, \DateTime $expires)
    {
        if ($method === 'POST') {
            return $this->generateSignaturePost($url, $expires);
        }
        
        $path = \parse_url($url, PHP_URL_PATH);

        $body = sprintf(
            "%s\n%s\n%s",
            $method,
            (string) $expires->format('U'),
            $path
            )
            ;
            
        $this->logger->debug('generate signature GET', 
            [ 'url' => $body, 'method' => 'GET' ]);
        
        return \hash_hmac('sha1', $body, $this->key, false);
    }

}
