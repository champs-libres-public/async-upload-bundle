<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\TempUrl;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface TempUrlGeneratorInterface
{
    const GET = 'GET';
    const POST = 'POST';
    
    public function generatePost(
        $expire_delay = null,
        $submit_delay = null,
        $max_file_count = 1
    );
    
    public function generate($method, $object_name, $expire_delay = null);
}
