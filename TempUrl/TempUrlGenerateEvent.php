<?php
/*
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\TempUrl;

use Symfony\Component\EventDispatcher\Event;

/**
 * 
 *
 */
class TempUrlGenerateEvent extends Event
{
    private $method;
    
    private $data;
    
    const NAME_GENERATE = 'async_uploader.generate_url';
    
    public function __construct($method, \stdClass $data)
    {
        $this->method = $method;
        $this->data = $data;
    }
    
    public function getMethod(): string
    {
        return $this->method;
    }

    public function getData(): \stdClass
    {
        return $this->data;
    }
}
