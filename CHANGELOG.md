Version 1.0.4
=============

- [minor] fix variable name;

Version 1.0.3
=============

- allow to connect to openstack version 3
- remove deps to rabbitmq

Version 1.0.2
=============

- add some docs in README.md

Version 1.0.1
=============

- upgrade deps for php opencloud

