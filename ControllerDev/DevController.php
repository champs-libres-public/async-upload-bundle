<?php
/*
 *
 *
 */
namespace ChampsLibres\AsyncUploaderBundle\ControllerDev;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorException;

/**
 * Class DevController
 * Contains some route useful in dev.
 *
 * @package ChampsLibres\AsyncUploaderBundle\ControllerDev
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class DevController extends AbstractController
{
   /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/asyncupload/dev/url/test_form/html")
     */
    public function testFormAction()
    {
        $r = $this->get('async_uploader.temp_url_generator')
            ->generatePost();

        return new \Symfony\Component\HttpFoundation\Response(<<<HTML
<html>
            <head>
            <title>Test upload form</title>
            <script type="text/javascript" src="/js/script_html.js"></script>
            <style> .thumb{width:160px;}</style>
            </head>
            <body>
            <form id="form" action="$r->url"
    method="POST"
    enctype="multipart/form-data">
    <input type="hidden" name="redirect" value=""/>
    <input type="hidden" name="max_file_size" value="$r->max_file_size"/>
    <input type="hidden" name="max_file_count" value="$r->max_file_count"/>
    <input type="hidden" name="expires" value="$r->expires"/>
    <input type="hidden" name="signature" value="$r->signature"/>
    <input type="file" name="tttt"/>
    <br/>
    <input type="submit"/>
</form>
<div><button onclick="javascript:ActionButton()">Go</button></div>
            </body>
            </html>
HTML
            );
    }

    /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/asyncupload/dev/url/test_form/ajax")
     */
    public function testFormAjaxAction(Request $request)
    {
        $form = $this->createFormBuilder(null, [
            'action' => $this->get('router.request_context')
                ->getPathInfo()
            ])
            ->add('async_upload',
            \ChampsLibres\AsyncUploaderBundle\Form\Type\AsyncUploaderFormType::class,
                ['max_files' => 3]
            )
            ->add('submit', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class)
            ->getForm()
            ;


        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            $r = new \stdClass;
            $r->message = "data arrived... Here is what I get";
            $r->data = $form->getData();

            return new JsonResponse($r);
        }

        return $this->render(
            'ChampsLibresAsyncUploaderBundle:_dev:test_form.html.twig',
            [
                'form' => $form->createView(),
                'form2' => $form->createView()
            ]
            );
    }
}
