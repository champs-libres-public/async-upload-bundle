<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Form\DataTransformer;

use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use ChampsLibres\AsyncUploaderBundle\Form\AsyncFileTransformer\AsyncFileTransformerInterface;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class JsonToModelDataTransformer implements DataTransformerInterface
{
    /**
     *
     * @var AsyncFileTransformerInterface
     */
    protected $transformer;
    
    /**
     *
     * @var integer
     */
    protected $max_files;
    
    /**
     *
     * @var TempUrlGeneratorInterface
     */
    protected $temp_url_generator;
    
    public function __construct(
        $max_files,
        TempUrlGeneratorInterface $temp_url_generator,
        AsyncFileTransformerInterface $transformer
    ) {
        $this->max_files = $max_files;
        $this->transformer = $transformer;
        $this->temp_url_generator = $temp_url_generator;
    }

    
    public function reverseTransform($json)
    { 
        if ($json === null) {
            return;
        }
        
        $files = \json_decode($json);
        
        if ($files === null || !\property_exists($files, 'files')) {
            return;
        }

        $asyncFiles = [];
        
        foreach($files->files as $file) {
            $asyncFiles[] = $this->transformer->toData(
                new class($file->object_name) implements AsyncFileInterface {
                    protected $object_name;
                
                    public function __construct($object_name) 
                    {
                        $this->object_name = $object_name;
                    }

                    public function getObjectName()
                    {
                        return $this->object_name;
                    }
                }
                );
        }
        
        return $this->max_files === 1 ? $asyncFiles[0] ?? null : $asyncFiles;
    }

    public function transform($file)
    { 
        if ($file === null) {
            return null;
        }
        
        if (
            $file instanceof \Traversable || is_array($file)
        ) {
            $r = new \stdClass;
            $r->files = [];
            
            foreach($file as $f) {
                $r->files[] = $this->transformAsyncFile($f);
            }
            
            return \json_encode($r);
        }
        
        if ($file instanceof AsyncFileInterface) {
            $r = new \stdClass;
            
            $r->files = [ $this->transformAsyncFile($file) ];
            
            return \json_encode($r);
        }
        
        throw new TransformationFailedException("The data does not implements "
            . "Traversable and Countable, nor ".AsyncFileInterface::class);
    }
    
    protected function transformAsyncFile(AsyncFileInterface $asyncFile)
    {
        return (object) [
            'object_name' => $asyncFile->getObjectName(),
            'url'         => $this->temp_url_generator
                ->generateGet($asyncFile->getObjectName())
                ->url
        ];
    }
    
}
