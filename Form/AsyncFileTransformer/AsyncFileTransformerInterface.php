<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Form\AsyncFileTransformer;

use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface AsyncFileTransformerInterface
{
    public function toAsyncFile($data);
    
    public function toData(AsyncFileInterface $asyncFile);
}
