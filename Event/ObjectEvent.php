<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Event;

use \Symfony\Component\EventDispatcher\Event;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ObjectEvent extends Event
{
    protected $object_name;
    
    const PERSISTED = 'async_uploader.persisted';
    
    public function __construct($object_name)
    {
        $this->object_name = $object_name;
    }
    
    public function getObjectName()
    {
        return $this->object_name;
    }



}
