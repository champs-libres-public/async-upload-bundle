<?php

namespace ChampsLibres\AsyncUploaderBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ChampsLibresAsyncUploaderExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $this->processPersistenceCheckerConfiguration($config, $container);
        $this->processOpenstackConfiguration($config, $container);
        $this->processAsyncUploadConfiguration($config, $container);
        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../config'));
        $loader->load('services.yaml');
    }
    
    protected function processPersistenceCheckerConfiguration($config, ContainerBuilder $container)
    {
        // persistence checker is not implemented any more.
        // see ChampsLibres\AsyncUploaderBundle\TempUrl\AsyncUploaderSignatureChecker
    }
    
    protected function processOpenstackConfiguration($config, ContainerBuilder $container)
    {
        foreach (\array_keys($config['openstack']) as $key) {
            $container
                ->setParameter(
                    'async_upload.openstack.'.$key, 
                    $config['openstack'][$key]
                    );
        }
    }
    
    protected function processAsyncUploadConfiguration($config, $container)
    {
        foreach (\array_keys($config['temp_url']) as $key) {
            $container
                ->setParameter(
                    'async_upload.temp_url.'.$key, $config['temp_url'][$key]
                    );
        }
    }
}
