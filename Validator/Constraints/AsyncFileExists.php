<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * 
 * @Annotation
 * 
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncFileExists extends Constraint
{
    public $message = "The file '{{ filename }}' is not stored properly.";
    
    public function validatedBy()
    {
        return AsyncFileExistsValidator::class;
    }
    
    public function getTargets()
    {
        return [ Constraint::CLASS_CONSTRAINT, Constraint::PROPERTY_CONSTRAINT ];
    }
}
