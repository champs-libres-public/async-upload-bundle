<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncFileExistsValidator extends ConstraintValidator
{
    /**
     *
     * @var TempUrlGeneratorInterface
     */
    private $tempUrlGenerator;
    
    /**
     *
     * @var \GuzzleHttp\Client
     */
    private $client;
    
    public function __construct(TempUrlGeneratorInterface $tempUrlGenerator)
    {
        $this->tempUrlGenerator = $tempUrlGenerator;
        $this->client = new \GuzzleHttp\Client();
    }
    
    public function validate($file, Constraint $constraint)
    {
        if ($file instanceof AsyncFileInterface) {
            $this->validateObject($file->getObjectName(), $constraint);
        } else {
            $this->validateObject($file, $constraint);
        }
    }
    
    protected function validateObject($file, Constraint $constraint)
    {
        $urlHead = $this->tempUrlGenerator->generate('HEAD', 
            $file, 30);
        
        try {
            $r = $this->client->request('HEAD', $urlHead->url);
        } catch (ClientException $e) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ filename }}', $file)
                ->addViolation()
                ;
        }
    }
    
}
