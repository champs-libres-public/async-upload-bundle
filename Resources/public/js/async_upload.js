// Create the form when page load
/*
document.addEventListener("DOMContentLoaded", function(event) {
  var
  asyncFilesInputs = document.querySelectorAll('input[type="hidden"][data-async-file-upload]');

  for (let i=0; i < asyncFilesInputs.length; i++) {
    asyncupload.buildForm(asyncFilesInputs[i], null);
  }
});
*/

// Create the asyncupload object that stores all JS functions for the AsyncUploaderBundle

var asyncupload = {

  ///////////////////////////////////////////////////
  // buildForm :  build the form for images upload //
  ///////////////////////////////////////////////////

  buildForm : function (asyncFileInput, thumbnailDiv) {
    var
    inputField, img,
    data = asyncFileInput.value === "" ? { "files" : [] } :
    JSON.parse(asyncFileInput.value),
    getUrl = asyncFileInput.dataset.getUrl,
    uploadedFilesLength = data.files.length,
    uniqid = asyncupload.makeid()
    ;

    if (thumbnailDiv === null) {
      thumbnailDiv = document.createElement("div");
      thumbnailDiv.classList.add("thumbnails");
      // insert in DOM after the asyncFileInput
      asyncFileInput.parentNode.appendChild(thumbnailDiv);

      // add eventual existing files
      for (let i = 0; i < uploadedFilesLength; i++) {
        asyncupload.getExistingUrlForImage(data.files[i], getUrl).then(function(url) {
            return window.fetch(url);
        }).then(function(r) {
          if (r.ok) {
            return r.blob();
          } else {
            throw Error('could not retrieve image: '+r.status);
          }
        }).then(function(blob) {
          asyncupload.makeThumbnailDiv(thumbnailDiv, blob);
        }).catch(function(err) {
          console.log(err);
        });
      }
    }

    if (uploadedFilesLength < Number(asyncFileInput.dataset.maxFiles)) {
      inputField = document.createElement("input");
      inputField.type = "file";
      inputField.id = "input_async_upload";
      inputField.name = uniqid;
      // listen for changes
      inputField.addEventListener("change", asyncupload.processInputField, false);
      asyncFileInput.parentNode.append(inputField);

    }

    // the name of the input field will be used to find which thumbnail and input belongs to...
    thumbnailDiv.dataset.inputName = uniqid;
    asyncFileInput.dataset.inputName = uniqid;
  },


  ////////////////////////////
  // getExistingUrlForImage //
  ////////////////////////////

  /**
  *
  * @param {Object} data the data of the current url
  * @param {string} getUrl the url to retrieve the temporary url, if not present
  *  in data. This url is in data-get-url="*" from the asyncFileInput
  * @returns {Promise}
  */
  getExistingUrlForImage : function (data, getUrl) {

    if (data.hasOwnProperty('url')) {
      return Promise.resolve(data.url);
    } else {
      return window.fetch(getUrl+'?'+encodeURIComponent('object_name')+'='+encodeURIComponent(data.object_name))
      .then(function(r) {
        if (r.ok) {
          return r.json();
        } else {
          throw Error("could not fetch the temporary url");
        }
      }).then(function(json) {
        return json.url;
      }).catch(function(err) {
        console.log("could not retrieve url", err);
      });
    }
  },


  ///////////////////////
  // processInputField //
  ///////////////////////

  processInputField : function (ev) {
    // Access the file object that was inputed.
    var
    fileInput = ev.target,
    uniqid = fileInput.name,
    fileObject = fileInput.files[0],
    thumbnailDiv = document.querySelector('div[data-input-name="'+uniqid+'"]'),
    asyncFileInput = document.querySelector('input[type="hidden"][data-input-name="'+uniqid+'"]'),
    formData = new FormData(),
    fileName = asyncupload.makeid(),
    jsonData,
    objectName,
    existingAsyncFileInputValueJson,
    url = asyncFileInput.dataset.tempUrl
    ;

    if (!fileObject.type.match('image.*')) {
      alert("Upload aborted. Please upload a correct image file");

      return;
    }


    // Get the asyncupload parameters
    window.fetch(url)
    .then(function(r) {
      // handle asyncupload parameters
      if (r.ok) {
        return r.json();
      } else {
        throw new Error('not ok');
      }
    }).then(function(data) {
      // upload to openstack swift
      if (fileObject.size > data.max_file_size){console.log("Upload file too large");}

      formData.append("redirect", data.redirect);
      formData.append("max_file_size", data.max_file_size);
      formData.append("max_file_count", data.max_file_count);
      formData.append("expires", data.expires);
      formData.append("signature", data.signature);
      formData.append("file", fileObject, fileName);

      // prepare the form data which will be used in next step
      objectName = data.prefix + fileName;

      //jsonData = { "object_name": objectName };
      jsonData = { "object_name": objectName };

      return window.fetch(data.url, {
        method: 'POST',
        mode: 'cors',
        body: formData
      });

    }).then(function(r) {
      if (r.ok) {
        console.log('Succesfully uploaded');

        // add object name to thumbnailDiv
        asyncupload.addObjectNameToThumbnailDiv(thumbnailDiv, objectName);

        // Update info in the form, as upload is successful
        if (asyncFileInput.value === "") {
          existingAsyncFileInputValueJson = { "files": [ jsonData ] };
        } else {
          existingAsyncFileInputValueJson = JSON.parse(asyncFileInput.value);
          existingAsyncFileInputValueJson.files.push(jsonData);
        }
        asyncFileInput.value = JSON.stringify(existingAsyncFileInputValueJson);

        // remove file input and build form again
        fileInput.parentNode.removeChild(fileInput);
        // recreate an input if possible (max_files not attained)
        asyncupload.buildForm(asyncFileInput, thumbnailDiv);
      } else {
        console.log('bad');
        console.log(r.status);
        // Handle errors

      }
    }).catch(function(err) {
      /* error :( */
      console.log("catch an error: " + err.name + " - " + err.message);
      alert("There was an error posting your images. Please try again.");
      throw new Error('openstack error: ' + err );
    });

    // Create a thumbnail view of the image of the fileObject inside thumbnailDiv
    asyncupload.makeThumbnailDiv(thumbnailDiv, fileObject);

  },

  //////////////////////
  // makeThumbnailDiv //
  //////////////////////

  makeThumbnailDiv : function(div, file) {
    var
    reader = new FileReader(file),
    img,
    button;

    img = document.createElement("img");
    img.classList.add("thumb");
    div.appendChild(img);

    button = document.createElement("button");
    // TODO: change the following line for better formatting the button innerHTML
    button.innerHTML = 'x';
    button.addEventListener("click", asyncupload.deleteInputFile, false);
    div.appendChild(button);


    reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
    reader.readAsDataURL(file);
  },

  ////////////
  // makeid //
  ////////////

  makeid : function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 7; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  },

  /////////////////////////////////////////////////////////////////////////////////////////////
  // addObjectNameToThumbnailDiv: This is to know which button is associated with which file //
  /////////////////////////////////////////////////////////////////////////////////////////////

  addObjectNameToThumbnailDiv : function (thumbnailDiv, objectName){
    var thumbnailBtn = thumbnailDiv.lastChild;

    // add the objectName to the button as data attributes
    thumbnailBtn.setAttribute("data-object-name", objectName);
  },

  /////////////////////
  // deleteInputFile //
  /////////////////////

  deleteInputFile : function (ev) {

    // Prevent the default action, which is form submitting
    ev.preventDefault();

    // Access the file object that was inputed and other variables
    var
    thumbnailBtn = ev.target,
    thumbnailImg = thumbnailBtn.previousSibling,
    objectName = thumbnailBtn.getAttribute("data-object-name"),
    thumbnailDiv = thumbnailBtn.parentNode,
    formId = thumbnailDiv.dataset.inputName,
    asyncFileInput = document.querySelector('input[type="hidden"][data-input-name="' + formId + '"]'),
    updatedAsyncFileInputValueJson = {"files": [] },
    existingAsyncFileInputValueJson,
    filesLength
    ;

    // remove the img of the thumbnail and the corresponding button
    thumbnailDiv.removeChild(thumbnailImg);
    thumbnailDiv.removeChild(thumbnailBtn);

    // remove the corresponding asyncFileInput value, if exists
    if (asyncFileInput.value) {
      existingAsyncFileInputValueJson = JSON.parse(asyncFileInput.value);
      filesLength = existingAsyncFileInputValueJson.files.length;
      for (let i = 0; i < filesLength; i++) {
        if (existingAsyncFileInputValueJson.files[i].object_name !== objectName) {
          updatedAsyncFileInputValueJson.files.push(existingAsyncFileInputValueJson.files[i]);
        }
      }
      // update the value of the input field
      asyncFileInput.value = JSON.stringify(updatedAsyncFileInputValueJson);
    } else {
      asyncFileInput.value = null;
    }

    // rebuild the form if needed
    asyncupload.buildForm(asyncFileInput,thumbnailDiv);

  }

};

export default asyncupload;
