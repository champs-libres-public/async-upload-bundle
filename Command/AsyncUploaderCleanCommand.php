<?php

namespace ChampsLibres\AsyncUploaderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AsyncUploaderCleanCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('async-uploader:clean')
            ->setDescription('...')
            
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cleaner = $this->getContainer()->get('async_uploader.cleaner');
        $cleaner->clean($input, $output);
        
        $output->writeln('Command result.');
    }

}
