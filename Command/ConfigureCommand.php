<?php

namespace ChampsLibres\AsyncUploaderBundle\Command;

use ChampsLibres\AsyncUploaderBundle\Openstack\Client;
use OpenStack\OpenStack;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ConfigureCommand extends Command
{
    private HttpClientInterface $client;

    private string $basePath;

    private string $tempUrlKey;

    public function __construct(HttpClientInterface $client, ParameterBagInterface $parameterBag)
    {
        $this->client = $client;
        $this->basePath = $parameterBag->get('async_upload.temp_url.temp_url_base_path');
        $this->tempUrlKey = $parameterBag->get('async_upload.temp_url.temp_url_key');

        parent::__construct();
    }


    protected function configure()
    {
        $this->setDescription('Configure openstack container to store documents')
            ->setName('async-upload:configure')
            ->addOption('os_token', 'o', InputOption::VALUE_REQUIRED, 'Openstack token')
            ->addOption('domain', 'd', InputOption::VALUE_IS_ARRAY|InputOption::VALUE_REQUIRED, 'Domain name')
        ;
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->hasOption('os_token')) {
            $output->writeln("The option os_token is required");

            throw new \RuntimeException("The option os_token is required");
        }

        if (0 === count($input->getOption('domain'))) {
            $output->writeln("At least one occurence of option domain is required");

            throw new \RuntimeException("At least one occurence of option domain is required");
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $domains = \implode(' ', $input->getOption('domain'));

        if ($output->isVerbose()) {
            $output->writeln(['Domains configured will be', $domains]);
        }

        try {
            $response = $this->client->request('POST', $this->basePath, [
                'headers' => [
                    'X-Auth-Token' => $input->getOption('os_token'),
                    'X-Container-Meta-Access-Control-Allow-Origin' => $domains,
                    'X-Container-Meta-Temp-URL-Key' => $this->tempUrlKey,
                ]
            ]);
        } catch (HttpExceptionInterface $e) {
            $output->writeln('Error');
            $output->writeln($e->getMessage());

            if ($output->isVerbose()) {
                $output->writeln($e->getTraceAsString());
            }
        }

        return 0;
    }
}
