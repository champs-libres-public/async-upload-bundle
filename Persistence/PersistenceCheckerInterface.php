<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Persistence;

/**
 * Check the persistence of an object in the database. 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface PersistenceCheckerInterface
{
    /**
     * 
     * @param string $object_name
     * @return bool
     */
    public function isPersisted($object_name);
}
