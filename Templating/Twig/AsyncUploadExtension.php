<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Templating\Twig;

use ChampsLibres\AsyncUploaderBundle\Model\AsyncFileInterface;
use ChampsLibres\AsyncUploaderBundle\TempUrl\TempUrlGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class AsyncUploadExtension extends AbstractExtension
{
    /**
     *
     * @var TempUrlGeneratorInterface
     */
    protected $urlGenerator;
    
    /**
     *
     * @var UrlGeneratorInterface
     */
    protected $routingUrlGenerator;
    
    public function __construct(
        TempUrlGeneratorInterface $tempUrlGenerator,
        UrlGeneratorInterface $routingUrlGenerator
    ) {
        $this->urlGenerator = $tempUrlGenerator;
        $this->routingUrlGenerator = $routingUrlGenerator;
    }
    
    public function getFilters()
    {
        return [
            new TwigFilter('file_url', [$this, 'computeUrl']),
            new TwigFilter('generate_url', [$this, 'computeGenerateUrl'])
        ];
    }
    
    public function computeUrl($file, $expiresDelay = null)
    {
        if ($file instanceof AsyncFileInterface) {
            $object_name = $file->getObjectName();
        } elseif (is_string($file)) {
            $object_name = $file;
        } else {
            throw new \InvalidArgumentException("The file should be an instance"
                . " of ".AsyncFileInterface::class." or string, ".
                \is_object($file) ? \get_class($file) : \gettype($file)
                ." given.");
        }
        
        return $this->urlGenerator->generateGet($object_name, $expiresDelay)->url;
    }
    
    public function computeGenerateUrl($file, $method = 'GET', $expiredDelay = null)
    {
        if ($file instanceof AsyncFileInterface) {
            $object_name = $file->getObjectName();
        } elseif (is_string($file)) {
            $object_name = $file;
        } else {
            throw new \InvalidArgumentException("The file should be an instance"
                . " of ".AsyncFileInterface::class." or string, ".
                \is_object($file) ? \get_class($file) : \gettype($file)
                ." given.");
        }
        
        return $this->routingUrlGenerator
            ->generate('async_upload.generate_url', [
                'method' => $method,
                'object_name' => $object_name
            ]);
    }
    
    
}
