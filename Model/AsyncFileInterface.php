<?php
/*
 * 
 * 
 */
namespace ChampsLibres\AsyncUploaderBundle\Model;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
interface AsyncFileInterface
{
    /**
     * @var string
     */
    public function getObjectName();
}
